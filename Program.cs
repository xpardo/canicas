﻿/**
*Trello: https://trello.com/b/wFNEAdPT/canica;
*Repositori: https://bitbucket.org/xpardo/canicas/src/master/
* Author: xenia
*/

using System;
using System.Collections.Generic;



class Program
{
    


    //recollim el nou numero de canicas de la persona en concret i pasem aquest numero a subMenu per a poder canviar l'array.
    public static int Consulta(string[] nens, int[] canicas , string[] ciutat, int posicio){
        Console.WriteLine("\n Ara aquesta es la cuantitat de canicas que tens:");
        Console.WriteLine("Nom: " + nens[posicio]+ "\ncanicas: " + canicas[posicio] + "\nPoblación: " + ciutat[posicio]);
        Console.WriteLine("\n Digam les canicas que bols posar:");
        int canicasActuals = Convert.ToInt32(Console.ReadLine());
        return canicasActuals;

    }
    //menú per al usuari, on es decideix que fer, si veure les seves canicas i tal o canviar el nom de canicas que té.

    public static int subMenu(string[] nens, int[] canicas , string[] ciutat, int posicio){
        Console.WriteLine("Que bols fer?");
        Console.WriteLine("\n1. saber el numero de canicas");
        Console.WriteLine("\n2. Actualitzar el numero de canicas");
        Console.WriteLine("\n3. Sortir");
        int opcio =Convert.ToInt32(Console.ReadLine());
        
        switch(opcio){
            //mostrem el contingut de l'array
            case 1:
                Console.WriteLine("\n Ara aquestas  la quantitat de canicas que tens:");
                Console.WriteLine("Nom: " + nens[posicio]+ "\ncanicas: " + canicas[posicio] + "\nPoblación: " + ciutat[posicio]);
            break;
            //Canviem el valor de las canicas
            case 2:
                canicas[posicio]=Consulta(nens,canicas,ciutat,posicio);
            break;
            
            //Basicament sortim del menú per a canviar de usuaris.
            case 3:
                Console.WriteLine("");
                Jugador();
            break;
         

            default:
                Console.WriteLine("Selecciona una opcion valida!\n");
            break;

        }
        return canicas[posicio];


    }
    //Preguntem el nom i busquem la posició on es troba aquest usuari per a poder modificar la resta de arrays.  
    public static void Jugador()
    {

        string[] nens ={"Joan","Roc", "Thiago", "Nico", "Noa", "Gerard", "Nor"}; 
        int[] canicas = {12,3,25,31,6,11,21,9};
        string[] ciutat = {"Vilanova","Canyelles","Vilanova","Sitges","Cunit","Vilanova","Kazakhstan","Cubelles"};
        //búcle infinit per a poder preguntar continuament l'usuari i poder accedir al programa.

        for(;;){
            Console.WriteLine("Escriu el teu nom");
            string nom = Console.ReadLine();
            
            int posicio = 0;
            int trobats = 0;

            for(int n = 0; n< nens.Length; n++){
                if(String.Equals(nom,nens[n])){
                    posicio =n;
                    trobats =1;
                }
            }

            if(trobats==0){
                Console.WriteLine("No s'ha pogut trobar cap nen amb aquest nom");
            }
            else{
                Console.WriteLine("s'ha tubat el nen");
                canicas[posicio] = subMenu(nens, canicas, ciutat, posicio); 
            }


        }
       
    }

    static void Main(string[] args){


        while (true){
            Console.WriteLine("\n que bols fer:");
            
            Console.WriteLine("\n 1. consultar jugador");
            int opcio =Convert.ToInt32(Console.ReadLine());

            switch(opcio){
               

                

                //consultar jugadors
               case 1:
                    Console.WriteLine("\n Trobar jugador");
                    Jugador();
                break;

                default:
                        Console.WriteLine("\nAgafa una opció!\n");
                break;
                
            }
        }

    }
  
}

